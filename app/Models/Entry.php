<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
  use HasFactory;

  protected $fillable = ['title', 'author', 'source', 'link', 'year', 'type', 'body', 'image', 'attribution', 'published', 'published_date'];
}
