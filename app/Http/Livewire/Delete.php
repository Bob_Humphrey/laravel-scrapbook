<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Livewire\WithFileUploads;

class Delete extends Component
{
  use WithFileUploads;

  public $entry;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $body;
  public $image;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;
  public $publishedSwitchLabel;

  public function delete()
  {
    $this->entry->delete();
    session()->flash('success', "Entry has been deleted: $this->title");
    return redirect("/unpublished");
  }

  public function mount($id)
  {
    $entry = Entry::find($id);
    $this->entry = $entry;
    $this->title = $entry->title;
    $this->author = $entry->author;
    $this->source = $entry->source;
    $this->link = $entry->link;
    $this->year = $entry->year;
    $this->type = $entry->type;
    $this->body = $entry->body;
    $this->previousImage = $entry->image;
    $this->attribution = $entry->attribution;
    $this->published = $entry->published;
    $this->published_date = $entry->published_date;
    $this->image = NULL;
  }

  public function render()
  {
    return view('livewire.delete');
  }
}
