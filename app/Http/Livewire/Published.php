<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;

class Published extends Component
{
  public $entries;

  public function unpublish($id)
  {
    $entry = Entry::find($id);
    $entry->published = FALSE;
    $entry->published_date = NULL;
    $entry->save();
    session()->flash('success', "Entry has been unpublished: $entry->title");
    return redirect("/unpublished");
  }

  public function mount()
  {
    $this->entries = Entry::select('id', 'title', 'author', 'image', 'created_at', 'published_date')->where('published', 1)->orderBy('published_date', 'DESC')->get();
  }

  public function render()
  {
    return view('livewire.list', ['list' => 'published']);
  }
}
