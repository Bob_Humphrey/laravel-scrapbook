<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;

class Blog extends Component
{
  public $entries;

  public function mount()
  {
    $entries = Entry::where('published', 1)->orderBy('published_date', 'DESC')->get();
    $collection = collect($entries);
    $this->entries = $collection->map(function ($entry) {
      $entry->formattedPubishedDate = Carbon::createFromDate($entry->published_date)->format('F j, Y');
      return $entry;
    });
  }

  public function render()
  {
    return view('livewire.blog');
  }
}
