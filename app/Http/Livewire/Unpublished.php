<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;

class Unpublished extends Component
{
  public $entries;

  public function publish($id)
  {
    $entry = Entry::find($id);
    $entry->published = TRUE;
    $entry->published_date = Carbon::today()->format('Y-m-d');
    $entry->save();
    session()->flash('success', "Entry has been published: $entry->title");
    return redirect("/published");
  }

  public function mount()
  {
    $this->entries = Entry::select('id', 'title', 'author', 'image', 'created_at', 'published_date')->where('published', 0)->orderBy('id')->get();
  }

  public function render()
  {
    return view('livewire.list', ['list' => 'unpublished']);
  }
}
