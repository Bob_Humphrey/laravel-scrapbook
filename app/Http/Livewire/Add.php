<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class Add extends Component
{
  use WithFileUploads;

  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $body;
  public $image;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;

  protected $rules = [
    'title' => 'required|string|min:3|max:100',
    'author' => 'sometimes|string|max:100',
    'source' => 'sometimes|string|max:200',
    'link' => 'sometimes|url|max:150',
    'year' => 'sometimes|date_format:Y|min:4',
    'type' => 'required|integer',
    'body' => 'required|string',
    'image' => 'sometimes|image',
    'attribution' => 'sometimes|string',
    'published' => 'boolean',
    'published_date' => 'sometimes|date_format:m-d-Y',
  ];

  public function add()
  {
    $imageName = NULL;
    $formattedPublishedDate = NULL;

    $this->validate();

    if (!is_null($this->published_date)) {
      $formattedPublishedDate = substr($this->published_date, 6) . '-' . substr($this->published_date, 0, 2) . '-' . substr($this->published_date, 3, 2);
    }

    if (!is_null($this->image)) {
      $interventionImage = Image::make($this->image->path());
      $imageName = Str::random(40) . '.' . $this->image->extension();
      $normalSizePath = storage_path() . '/app/public/postimages/';
      $thumbnailPath = storage_path() . '/app/public/thumbnails/';
      // Normal size
      $interventionImage->resize(500, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($normalSizePath . $imageName);
      // Thumbnail
      $interventionImage->resize(100, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($thumbnailPath . $imageName);
    }

    $entry = Entry::create([
      'author' => $this->author,
      'source' => $this->source,
      'link' => $this->link,
      'year' => $this->year,
      'type' => $this->type,
      'body' => $this->body,
      'image' => $imageName,
      'attribution' => $this->attribution,
      'published' => $this->published,
      'published_date' => $formattedPublishedDate,
      'title' => Str::title($this->title),
    ]);

    $id = $entry->id;

    session()->flash('success', "New entry has been added: $this->title");
    return redirect("/unpublished");
  }

  public function mount()
  {
    $this->published = FALSE;
    $this->body = '';
    $this->image = NULL;
    $this->attribution = NULL;
    $this->previousImage = NULL;
    $this->publishedSwitchLabel = 'Not Published';
  }


  public function render()
  {
    return view('livewire.add');
  }
}
