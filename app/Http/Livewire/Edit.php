<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

use function PHPUnit\Framework\isNull;

class Edit extends Component
{
  use WithFileUploads;

  public $entry;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $body;
  public $image;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;
  public $publishedSwitchLabel;

  protected $rules = [
    'title' => 'required|string|min:3|max:100',
    'author' => 'nullable|string|max:100',
    'source' => 'nullable|string|max:200',
    'link' => 'nullable|url|max:150',
    'year' => 'nullable|date_format:Y|min:4',
    'type' => 'required|integer',
    'body' => 'required|string',
    'image' => 'nullable|image',
    'attribution' => 'nullable|string',
    'published' => 'boolean',
    'published_date' => 'nullable|date_format:m-d-Y',
  ];

  public function edit()
  {
    $imageName = $this->previousImage;
    $formattedPublishedDate = NULL;

    $this->validate();

    if (!is_null($this->published_date)) {
      $formattedPublishedDate = substr($this->published_date, 6) . '-' . substr($this->published_date, 0, 2) . '-' . substr($this->published_date, 3, 2);
    }

    if (!is_null($this->image)) {
      $interventionImage = Image::make($this->image->path());
      $imageName = Str::random(40) . '.' . $this->image->extension();
      $normalSizePath = storage_path() . '/app/public/postimages/';
      $thumbnailPath = storage_path() . '/app/public/thumbnails/';
      // Normal size
      $interventionImage->resize(500, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($normalSizePath . $imageName);
      // Thumbnail
      $interventionImage->resize(100, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($thumbnailPath . $imageName);
    }

    $entry = $this->entry;
    $entry->title = Str::title($this->title);
    $entry->author = $this->author;
    $entry->source = $this->source;
    $entry->link = $this->link;
    $entry->year = $this->year;
    $entry->type = $this->type;
    $entry->body = $this->body;
    $entry->image = $imageName;
    $entry->attribution = $this->attribution;
    $entry->published = $this->published;
    $entry->published_date = $formattedPublishedDate;
    $entry->save();

    session()->flash('success', "Entry has been updated: $this->title");
    return redirect("/unpublished");
  }

  public function mount($id)
  {
    $entry = Entry::find($id);
    $this->entry = $entry;
    $this->title = $entry->title;
    $this->author = $entry->author;
    $this->source = $entry->source;
    $this->link = $entry->link;
    $this->year = $entry->year;
    $this->type = $entry->type;
    $this->body = $entry->body;
    $this->previousImage = $entry->image;
    $this->attribution = $entry->attribution;
    $this->published = $entry->published;
    if (is_null($entry->published_date)) {
      $this->published_date = NULL;
    } else {
      $this->published_date = substr($entry->published_date, 5, 5) . '-' . substr($entry->published_date, 0, 4);
    }

    $this->image = NULL;
  }

  public function render()
  {
    return view('livewire.edit');
  }
}
