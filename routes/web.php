<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{page?}', function () {
//   return view('welcome');
// });

Route::get('/add', \App\Http\Livewire\Add::class)->name('add');
Route::get('/edit/{id}', \App\Http\Livewire\Edit::class)->name('edit');
Route::get('/view/{id}', \App\Http\Livewire\View::class)->name('view');
Route::get('/delete/{id}', \App\Http\Livewire\Delete::class)->name('delete');
Route::get('/published', \App\Http\Livewire\Published::class)->name('published');
Route::get('/unpublished', \App\Http\Livewire\Unpublished::class)->name('unpublished');
Route::get('/{page?}', \App\Http\Livewire\Blog::class)->name('blog');
