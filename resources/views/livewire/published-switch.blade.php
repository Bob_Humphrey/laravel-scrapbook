<div>
  <div class="relative inline-block w-12 mr-2 align-middle select-none transition duration-200 ease-in">
    <input type="checkbox" name="published" id="published" wire:model="published"
      class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 border-gray-500 appearance-none cursor-pointer" />
    <label for="published"
      class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-500 cursor-pointer"></label>
  </div>
  <label for="published" class="text-gray-700 uppercase pr-4">
    {{ $published ? 'Published' : 'Not Published' }}
  </label>
</div>
