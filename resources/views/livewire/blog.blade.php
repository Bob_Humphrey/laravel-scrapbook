<div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-3/5 mx-auto ">
  @foreach ($this->entries as $entry)

    <div class="flex justify-between items-end border-b border-gray-400 pb-2 mb-4">
      <h2 class="font-cabin text-4xl text-blue-500">
        {{ $entry->title }}
      </h2>
      <div class="text-gray-600 text-right">
        {{ $entry->formattedPubishedDate }}
      </div>
    </div>

    <div class="w-full grid grid-cols-7 gap-x-20 mb-40 mx-auto">

      <div class="col-span-4">
        <div class="font-nunito_semibold text-xl text-gray-900 text-justify mb-4">
          {!! $entry->body !!}
        </div>

        <div class=" text-gray-600 text-base space-y-1">
          @if ($entry->author)
            <div class="text-xl">
              {{ $entry->author }}
            </div>
          @endif

          @if ($entry->source)
            @if ($entry->link)
              <div>
                <a href="{{ $entry->link }}" target="_blank" rel="noopener noreferrer">
                  {!! $entry->source !!}
                </a>
              </div>
            @else
              <div class="">
                {!! $entry->source !!}
              </div>
            @endif
          @endif

          @if ($entry->year)
            <div class="">
              {{ $entry->year }}
            </div>
          @endif
        </div>

      </div>

      <div class="col-span-3">
        @if ($entry->image)
          <div>
            <img src={{ asset('storage/postimages/' . $entry->image) }} />
          </div>
        @endif
      </div>

    </div>

  @endforeach

</div>
