<div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">


  <h2 class="w-full heading text-center pb-6">
    Add Entry
  </h2>

  @include('livewire.form', ['disabled' => '', 'showImage' => FALSE])

  {{-- SUBMIT BUTTON --}}

  <div class="flex justify-center pt-3">
    <button type="submit" wire:click="add" class="button mx-auto">
      Add
    </button>
  </div>

</div>
