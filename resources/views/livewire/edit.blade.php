<div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

  <h2 class="w-full heading text-center pb-6">
    Edit Entry
  </h2>

  @include('livewire.form', ['disabled' => '', 'showImage' => TRUE])

  {{-- SUBMIT BUTTON --}}

  <div class="flex justify-center pt-3">
    <button type="submit" wire:click="edit" class="button mx-auto">
      Edit
    </button>
  </div>

</div>
