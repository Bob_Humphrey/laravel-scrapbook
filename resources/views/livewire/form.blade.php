<div class="w-full lg:w-9/10 md:grid grid-cols-12 gap-x-12 mx-auto">

  {{-- FIRST COLUMN --}}

  <div class="col-span-4">

    {{-- PUBLISHED --}}

    <div class="pb-3">
      <div class="relative inline-block w-12 mr-2 align-middle select-none transition duration-200 ease-in">
        <input type="checkbox" name="published" id="published" wire:model="published"
          class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 border-red-600 appearance-none cursor-pointer" />
        <label for="published"
          class="toggle-label block overflow-hidden h-6 rounded-full bg-red-600 cursor-pointer"></label>
      </div>
      <label for="published" class="{{ $published ? 'text-green-600' : 'text-red-600' }} uppercase pr-4">
        {{ $published ? 'Published' : 'Not Published' }}
      </label>
    </div>

    {{-- PUBLISHED DATE --}}

    <x-input-group name="published_date">
      @if ($disabled)
        <x-datepicker name="published_date" id="published_date" wire:model="published_date"
          class="p-2 rounded border border-gray-400 w-full appearance-none" placeholder="MM-DD-YYYY" disabled />
      @else
        <x-datepicker name="published_date" id="published_date" wire:model="published_date"
          class="p-2 rounded border border-gray-400 w-full appearance-none" placeholder="MM-DD-YYYY" />
      @endif
    </x-input-group>

    {{-- IMAGE --}}

    @if ($showImage === true && !is_null($previousImage))
      <div>
        <img src={{ asset('storage/postimages/' . $previousImage) }} />
      </div>
    @endif


  </div>

  {{-- SECOND COLUMN --}}

  <div class="col-span-4">

    {{-- TITLE --}}

    <x-input-group name="title">
      @if ($disabled)
        <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-400 w-full appearance-none"
          disabled />
      @else
        <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- AUTHOR --}}

    <x-input-group name="author">
      @if ($disabled)
        <x-input name="author" wire:model="author" class="p-2 rounded border border-gray-400 w-full appearance-none"
          disabled />
      @else
        <x-input name="author" wire:model="author" class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- SOURCE --}}

    <x-input-group name="source">
      @if ($disabled)
        <x-textarea name="source" wire:model="source" rows="3"
          class="p-2 rounded border border-gray-400 w-full appearance-none" disabled />
      @else
        <x-source wire:model.lazy="source" id="source" :initial-value="$source" />
      @endif
    </x-input-group>

    {{-- LINK --}}

    <x-input-group name="link">
      @if ($disabled)
        <x-input name="link" wire:model="link" class="p-2 rounded border border-gray-400 w-full appearance-none"
          disabled />
      @else
        <x-input name="link" wire:model="link" class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- IMAGE UPLOAD --}}

    <x-input-group name="image">
      @if ($disabled)
        <x-input type="file" name="image" wire:model="image"
          class="p-2 rounded border border-gray-400 w-full appearance-none" disabled />
      @else
        <x-input type="file" name="image" wire:model="image"
          class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- ATTRIBUTION --}}

    <x-input-group name="attribution">
      @if ($disabled)
        <x-textarea name="attribution" wire:model="attribution"
          class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" disabled />
      @else
        <x-textarea name="attribution" wire:model="attribution"
          class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" />
      @endif
    </x-input-group>

    {{-- YEAR AND TYPE --}}

    <div class="grid grid-cols-2 gap-x-8">

      {{-- YEAR --}}

      <x-input-group name="year">
        @if ($disabled)
          <x-input name="year" wire:model="year" class="p-2 rounded border border-gray-400 w-full appearance-none"
            disabled />
        @else
          <x-input name="year" wire:model="year" class="p-2 rounded border border-gray-400 w-full appearance-none" />
        @endif
      </x-input-group>

      {{-- TYPE --}}

      {{-- TINY INT - 127 MAX VALUE --}}

      <x-input-group name="type">
        <select name="type" id="type" wire:model="type"
          class="p-2 rounded border border-gray-400 w-full appearance-none" {{ $disabled }}>
          <option value="">Pick a type...</option>
          <option value="10">Article</option>
          <option value="15">Bible</option>
          <option value="20">Book</option>
          <option value="30">Essay</option>
          <option value="40">Movie</option>
          <option value="50">Novel</option>
          <option value="60">Poem</option>
          <option value="70">Quote</option>
          <option value="80">Song</option>
          <option value="90">Story</option>
          <option value="100">TV</option>
          <option value="110">Tweet</option>
        </select>
      </x-input-group>

    </div>

  </div>

  {{-- THIRD COLUMN --}}

  <div class="col-span-4">

    <div x-data="{
      openTab: 1,
      activeClasses: 'border-l border-t border-r rounded-t border-gray-400',
      inactiveClasses: 'hover:text-blue-500'
    }" class="">
      <ul class="flex border-b border-gray-400 text-sm text-gray-700">
        <li @click="openTab = 1" :class="{ '-mb-px': openTab === 1 }" class="-mb-px mr-1">
          <a :class="openTab === 1 ? activeClasses : inactiveClasses" class="bg-white inline-block py-2 px-4" href=" #">
            TEXT
          </a>
        </li>
        <li @click="openTab = 2" :class="{ '-mb-px': openTab === 2 }" class="mr-1">
          <a :class="openTab === 2 ? activeClasses : inactiveClasses" class="bg-white inline-block py-2 px-4" href="#">
            EMBEDDED CONTENT
          </a>
        </li>
      </ul>
      <div class="w-full pt-4">
        <div x-show="openTab === 1">
          <x-input-group name="body">
            @if ($disabled)
              <x-textarea name="body" wire:model="body"
                class="p-2 rounded border border-gray-400 w-full appearance-none" row="10" disabled />
            @else
              <x-body wire:model.lazy="body" id="body" :initial-value="$body" />
            @endif
          </x-input-group>
        </div>
        <div x-show="openTab === 2">
          <x-input-group name="body">
            @if ($disabled)
              <x-textarea name="body" wire:model="body"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" disabled />
            @else
              <x-textarea name="body" wire:model="body"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" />
            @endif
          </x-input-group>
        </div>
      </div>
    </div>

  </div>

</div>
