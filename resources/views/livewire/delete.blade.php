<div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

  <h2 class="w-full heading text-center pb-6">
    Delete Entry
  </h2>

  <div class="font-nunito_bold text-red-600 text-center pb-6">
    Warning! This action cannot be undone.
  </div>

  @include('livewire.form', ['disabled' => 'disabled', 'showImage' => TRUE])

  {{-- SUBMIT BUTTON --}}

  <div class="flex justify-center pt-3">
    <button type="submit" wire:click="delete" class="button mx-auto">
      Delete
    </button>
  </div>

</div>
