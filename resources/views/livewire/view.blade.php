<div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">

  <h2 class="w-full heading text-center pb-6">
    View Entry
  </h2>

  @include('livewire.form', ['disabled' => 'disabled', 'showImage' => TRUE])

  {{-- LINKS TO OTHER ACTIONS --}}

  <div class="flex flex-col lg:flex-row items-center justify-center w-full pt-3 ">
    <a href={{ url('edit/' . $entry->id) }} class="button lg:mx-4">
      Edit
    </a>
    <a href={{ url('delete/' . $entry->id) }} class="button lg:mx-4">
      Delete
    </a>
  </div>

</div>
