<div class="w-11/12 sm:w-10/12 md:w-7/12 lg:w-10/12 mx-auto ">


  <h2 class="w-full heading text-center pb-6">
    @if ($list === 'unpublished')
      Unpublished
    @else
      Published
    @endif
  </h2>

  <table class="w-11/12 mx-auto">
    @foreach ($this->entries as $entry)
      @if ($loop->index % 14 === 0)
        <tr class="grid grid-cols-12 gap-x-12 text-sm font-nunito_bold border-b border-gray-300">
          <th class="col-span-1 text-left py-3">
            @if ($list === 'unpublished')
              Draft
            @else
              Published
            @endif
          </th>
          <th class="col-span-1 text-left py-3">Image</th>
          <th class="col-span-4 text-left py-3">Title</th>
          <th class="col-span-3 text-left py-3">Author</th>
          <th class="col-span-3 text-center py-3">Actions</th>
        </tr>
      @endif
      @php
        $draftDate = substr($entry->created_at, 5, 5) . '-' . substr($entry->created_at, 0, 4);
        $publishedDate = substr($entry->published_date, 5, 5) . '-' . substr($entry->created_at, 0, 4);
      @endphp
      <tr
        class="grid grid-cols-12 gap-x-12 text-sm font-nunito_light {{ $loop->last ? '' : 'border-b border-gray-300' }}">
        <td class="col-span-1 flex items-center text-left py-3">
          @if ($list === 'unpublished')
            {{ $draftDate }}
          @else
            {{ $publishedDate }}
          @endif
        </td>
        <td class="col-span-1 text-left py-3">
          @if ($entry->image)
            <img src={{ asset('storage/postimages/' . $entry->image) }} />
          @endif
        </td>
        <td class="col-span-4 flex items-center text-left py-3">
          {{ $entry->title }}
        </td>
        <td class="col-span-3 flex items-center text-left py-3">
          {{ $entry->author }}
        </td>
        <td class="col-span-3 flex items-center justify-around text-left py-3">
          <div class="tooltip">
            <span class="tooltip-text">View</span>
            <a href={{ url('view/' . $entry->id) }}>
              <x-heroicon-o-eye class="w-7 link" />
            </a>
          </div>
          @if ($list === 'unpublished')
            <div class="tooltip">
              <span class="tooltip-text">Publish</span>
              <div wire:click="publish({{ $entry->id }})">
                <x-zondicon-add-outline class="w-5 link" />
              </div>
            </div>
          @else
            <div class="tooltip">
              <span class="tooltip-text">Unpublish</span>
              <div wire:click="unpublish({{ $entry->id }})">
                <x-zondicon-minus-outline class="w-5 link" />
              </div>
            </div>
          @endif
          <div class="tooltip">
            <span class="tooltip-text">Edit</span>
            <a href={{ url('edit/' . $entry->id) }}>
              <x-zondicon-edit-pencil class="w-5 link" />
            </a>
          </div>
          <div class="tooltip">
            <span class="tooltip-text">Delete</span>
            <a href={{ url('delete/' . $entry->id) }}>
              <x-zondicon-close-outline class="w-5 link" />
            </a>
          </div>

        </td>
      </tr>
    @endforeach
  </table>

</div>
