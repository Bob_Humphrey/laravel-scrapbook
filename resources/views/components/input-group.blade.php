    <div class="pb-3">
      <div class="text-gray-700 text-sm uppercase">
        <x-label for="{{ $name }}" />
      </div>
      <div class="">
        {{ $slot }}
      </div>
      <div class="">
        <x-error field="{{ $name }}" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>
