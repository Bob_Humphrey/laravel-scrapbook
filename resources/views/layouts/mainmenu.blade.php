<a href="{{ url('/add') }}" class="link lg:px-2 py-2">
  Add
</a>
<a href="{{ url('/published') }}" class="link lg:px-2 py-2">
  Published
</a>
<a href="{{ url('/unpublished') }}" class="link lg:px-2 py-2">
  Unpublished
</a>
<a href="{{ url('/') }}" class="link lg:px-2 py-2">
  Link
</a>
<a href="{{ url('/') }}" class="link lg:px-2 py-2">
  Link
</a>

@guest
  <a href="{{ route('register') }}" class="link lg:px-2 py-2">
    Sign Up
  </a>
  <a href="{{ route('login') }}" class="link lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/logout') }}" class="link lg:px-2 py-2">
    Logout
  </a>
@endauth
