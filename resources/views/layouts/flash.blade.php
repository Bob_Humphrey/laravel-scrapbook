<x-alert type="success" class="bg-green-700 text-white text-center p-4 mx-auto" />
<x-alert type="warning" class="bg-yellow-700 text-white text-center p-4 mx-auto" />
<x-alert type="danger" class="bg-red-700 text-white text-center p-4 mx-auto" />
