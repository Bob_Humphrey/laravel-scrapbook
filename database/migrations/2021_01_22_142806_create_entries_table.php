<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('entries', function (Blueprint $table) {
      $table->id();
      $table->string('title', 100)->index();
      $table->string('author', 100)->index();
      $table->string('source', 200)->nullable($value = true);
      $table->string('link', 150)->nullable($value = true);
      $table->string('year', 4)->nullable($value = true);
      $table->tinyInteger('type')->index();
      $table->text('body');
      $table->string('image', 100)->nullable($value = true);
      $table->boolean('published')->index();
      $table->date('published_date')->nullable($value = true)->index();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('entries');
  }
}
